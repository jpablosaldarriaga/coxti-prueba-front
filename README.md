1) Se crea el proyecto para el front-end
2) Se realizan los servicios, auth para el register() y el login() apuntando a las rutas asignadas en el back-end
3) Se crea la carpeta models que contiene la interface del usuario para asignar el tipo de los datos.
4) Dentro de los compoentes se crea la carpeta auth que contiene la carpeta login con los login.component y la carpeta register con los register.component.
5) Para la parte web se va usar Angular Material, por lo tanto se crea la carpeta shared que contiene el modulo shared para realizar las importaciones y exportaciones de los componentes de Angular Material.
6) En el register.component.ts se crean los metodos: register() que se comunica con su servicio para registrar a los usuarios, createForm() para asignar los validadores del formulario, se crean los validadores y el seteo de usuario setUser().
7) En el register.component.html se crea el formulario en la parte web y se conecta con el component.ts.
8) En el login.component.ts se crean los validadores para el ingreso de los usuarios en el constructor, se crean los metodos login() para validar el ingreso de los usuarios y almacenar sus datos en el localStorage para ser mostrados en el home, se crean los validadores, el seteo de los usuarios setUser(), y se crea el metodo error() para mostrar un snackBar en caso de que el ingreso sea invalido.
9) Se crea la carpeta main dentro de los componentes que contiene la carpeta home con los home.component y la carpeta pop-up que contiene los popup.component
10) En el home.component.ts dentro del metodo ngOnInit() se obtiene la data del usuario que ingresó para ser mostrados, el metodogetSalary() para obtener el salario de la persona, openDialog() que desplega el Dialog o Modal y el metodo closeSesion() para cerrar la sesión limpiando el localStorage y regresando al login.
11) En el popup.component se encuentran las importaciones necesarias para desplegar el Modal.


Los principales inconvenientes a la hora de realizar el proyecto fueron el desconocimiento que se tenía sobre las tecnologias que se debían emplear, además de errores relacionados con los cors que se solucionó importanto la libreria de cors de adonis. Otro problema fue al momento de subir el proyecto a git ya que la parte del front presentaba errores al momento de subirlo.
