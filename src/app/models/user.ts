//Modelo del usuario, email y password son datos obligatorios el resto opcionales

export interface User {
    email: string;
    password: string;
    username?: string;
    name?: string;
    iduser?: number;
    cel?: number;
    depart?: string;
    city?: string;
    neigh?: string;
    adress?: string;
    salary?: number;
    other?: number;
    montly?: number;
    financial?: number
}
