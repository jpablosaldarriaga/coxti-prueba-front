import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

//Servicio para la API de salario

export class SalaryService {
  apiURL = environment.apiURL;

  constructor(private http: HttpClient) { }

  salaryCalc(data: any): Observable<any> {
    let salario = {"salary": data}
    return this.http.post(`${this.apiURL}salary`, salario)
  }
}
