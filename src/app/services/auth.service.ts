import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})

//Servicio de la autenticación para el registro y el login

export class AuthService {
  apiURL = environment.apiURL;

  constructor(private http: HttpClient) { }

  register(user: User): Observable<any>{
    return this.http.post(`${this.apiURL}Users/create`, user);
  }

  login(user: User): Observable<any>{
    return this.http.post(`${this.apiURL}login`, user);
  }

 /* getDataUser(id: any): Observable<any>{
    const httpOptions = { headers: new HttpHeaders({ 'Bearer': 'application/json; charset=utf-8' })};
    return this.http.post(`${this.apiURL}Users/create`, id, httpOptions);
  }*/
}
