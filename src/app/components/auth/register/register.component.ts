import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthService } from '../../../services/auth.service';
import { successDialog, errorMessage } from '../../../functions/alerts';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
//Este componente maneja el registro de usuarios
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;
  user!: User;
  constructor(private fb:FormBuilder, private authService:AuthService, private router: Router) { 
    this.createForm();
  }

  ngOnInit(): void {
  }

  //Metodo que realiza el registro, comunicandose con la API 
  register():void {
    if(this.registerForm.invalid){
      return Object.values(this.registerForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }else{
      this.setUser();
      this.authService.register(this.user).subscribe((data: any) => {
        //successDialog('Registro completado con éxito');
        this.router.navigate(['/login']);
      }, error => {
        //errorMessage('Ha ocurrido un error');
      });
    }
  }

  //Validadores para el formulario de registro
  createForm(): void {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')]],
      password: ['', [Validators.required]],
      password2: ['', [Validators.required]],
      name: ['', [Validators.required]],
      username: ['', [Validators.required]],
      iduser: ['', [Validators.required]],
      cel: ['', [Validators.required]],
      depart: ['', [Validators.required]],
      city: ['', [Validators.required]],
      neigh: ['', [Validators.required]],
      adress: ['', [Validators.required]],
      salary: ['', [Validators.required]],
      other: ['', [Validators.required]],
      montly: ['', [Validators.required]],
      financial: ['', [Validators.required]]
    })
  }


  //se crean los validadores
  get emailValidate(){
    return(
      this.registerForm.get('email')?.invalid && this.registerForm.get('email')?.touched
    );
  }

  get passwordValidate(){
    return(
      this.registerForm.get('password')?.invalid && this.registerForm.get('password')?.touched
    );
  }

  get password2Validate(){
    const pass = this.registerForm.get('password')?.value;
    const pass2 = this.registerForm.get('password2')?.value;

    return pass === pass2 ? false : true;
  }

  get usernameValidate(){
    return(
      this.registerForm.get('username')?.invalid && this.registerForm.get('username')?.touched
    );
  }

  get nameValidate(){
    return(
      this.registerForm.get('name')?.invalid && this.registerForm.get('name')?.touched
    );
  }

  get iduserValidate(){
    return(
      this.registerForm.get('iduser')?.invalid && this.registerForm.get('iduser')?.touched
    );
  }

  get celValidate(){
    return(
      this.registerForm.get('cel')?.invalid && this.registerForm.get('cel')?.touched
    );
  }

  get departValidate(){
    return(
      this.registerForm.get('depart')?.invalid && this.registerForm.get('depart')?.touched
    );
  }

  get cityValidate(){
    return(
      this.registerForm.get('city')?.invalid && this.registerForm.get('city')?.touched
    );
  }

  get neighValidate(){
    return(
      this.registerForm.get('neigh')?.invalid && this.registerForm.get('neigh')?.touched
    );
  }

  get adressValidate(){
    return(
      this.registerForm.get('adress')?.invalid && this.registerForm.get('adress')?.touched
    );
  }

  get salaryValidate(){
    return(
      this.registerForm.get('salary')?.invalid && this.registerForm.get('salary')?.touched
    );
  }

  get otherValidate(){
    return(
      this.registerForm.get('other')?.invalid && this.registerForm.get('other')?.touched
    );
  }

  get montlyValidate(){
    return(
      this.registerForm.get('montly')?.invalid && this.registerForm.get('montly')?.touched
    );
  }

  get financialValidate(){
    return(
      this.registerForm.get('financial')?.invalid && this.registerForm.get('financial')?.touched
    );
  }

  //Seteo del usuario
  setUser(): void{
    this.user = {
      email: this.registerForm.get('email')?.value,
      password: this.registerForm.get('password')?.value,
      username: this.registerForm.get('username')?.value,
      name: this.registerForm.get('name')?.value,
      iduser: this.registerForm.get('iduser')?.value,
      cel: this.registerForm.get('cel')?.value,
      depart: this.registerForm.get('depart')?.value,
      city: this.registerForm.get('city')?.value,
      neigh: this.registerForm.get('neigh')?.value,
      adress: this.registerForm.get('adress')?.value,
      salary: this.registerForm.get('salary')?.value,
      other: this.registerForm.get('other')?.value,
      montly: this.registerForm.get('montly')?.value,
      financial: this.registerForm.get('financial')?.value
    }
  }
}
