import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { AuthService } from '../../../services/auth.service';
import { successDialog, errorMessage } from '../../../functions/alerts';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
//Componente que maneja el login y obtiene los datos del usuario para mostrarlos en el home
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  user!: User;
  loading!: false;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private _snackBar: MatSnackBar) { 
    //Validadores del login
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')]],
      password: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  //Metodo login se comunica con la API y obtiene los datos de token y user para ser mostrados en el home
  login(): void {
    if(this.loginForm.invalid){
      return Object.values(this.loginForm.controls).forEach(control => {
        control.markAsTouched();
      });
    }else{
      this.setUser();
      this.authService.login(this.user).subscribe((data:any) => {
        //successDialog('Loggeo exitoso');
        console.log('data', data)
        localStorage.setItem('token', JSON.stringify(data[0].token));
        localStorage.setItem('user', JSON.stringify(data[1][0])); 
        this.router.navigate(['home']);
      }, error => {
        console.log(error);
        //errorMessage('Email o contraseña inavlidos');
        this.error();
      });
    }
  }

  //Se crean los validadores

  get emailValidate(){
    return(
      this.loginForm.get('email')!.invalid && this.loginForm.get('email')!.touched
    );
  }

  get passwordValidate(){
    return(
      this.loginForm.get('password')!.invalid && this.loginForm.get('password')!.touched
    );
  }

  //Seteo del usuario
  setUser(): void{
    this.user = {
      email: this.loginForm.get('email')!.value,
      password: this.loginForm.get('password')!.value
    }
  }

  //Error para mostrar un snackBar en caso de que este ocurra
  error(): void {
    this._snackBar.open('El email o la contraseña son invalidos', '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    });
  }

}
