import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { SalaryService } from '../../../services/salary.service';
import { errorMessage } from '../../../functions/alerts';
import { MatDialog } from '@angular/material/dialog';
import { PopUpComponent } from '../pop-up/pop-up.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
//En este componente se muestran los datos del usuario al momento de loggearse
export class HomeComponent implements OnInit {
  user!: User;
  salaryService!: SalaryService;

  constructor( private _snackBar: MatSnackBar, private router: Router, public dialog: MatDialog) { }

  //Se obtienen los datos en el ngOnInit desde el login
  ngOnInit(): void {
    let userData = localStorage.getItem('user');
    let data = JSON.parse(userData!);
    this.user = data;
    console.log('Data usuario:', data);
  }

  //Se obtiene el salario desde su servicio
  getSalary(): void {
    let sueldo = localStorage.getItem('salary');
    this.salaryService.salaryCalc(sueldo).subscribe((data: any) => {
      console.log(data)
      localStorage.setItem('res', JSON.stringify(data));
    this.openDialog();
    }, error => {
      console.log(error)
      errorMessage('Error de calculo');
    })
  }

  //Desplegar el Dialog o Modal
  openDialog(){
    let resCal = localStorage.getItem('res');
    const dialog = this.dialog.open(PopUpComponent);
    dialog.componentInstance.data = JSON.parse(resCal!);
  }

  //Cerrar sesión, limpia el localStorage y vuelve al login
  closeSesion(): void {
    localStorage.clear();
    this._snackBar.open('La sesión ha sido cerrada', '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    });
    this.router.navigate(['login']);
  }

}
